package simple_search.swing;

import javax.swing.SwingUtilities;

import common.Mazes;
import simple_search.common.MazeModel;
import simple_search.common.MazeSearch;
import simple_search.common.SearchController;
import simple_search.common.View;

/**
 *
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class SwingController implements SearchController{

	private final SwingView view;
	private final MazeModel model;
	private final MazeSearch mazeSearch;

	protected SwingController(int[][] maze)	{

		model = new MazeModel(maze);
		mazeSearch = new MazeSearch(model);
		mazeSearch.setDelay(100);

		view = new SwingView(model);
		view.setSearchTypes(searchTypeNames());
		view.addActionListener(e->search());
		view.showGui();
	}

	@Override
	public void updateInfo(String text){
		SwingUtilities.invokeLater(()-> getView().info(text));
	}

	@Override
	public void clearInfo(){
		SwingUtilities.invokeLater(()-> getView().clearInfo());
	}

	/**
	* Get {@link #view}
	*/
	@Override
	public View getView() {
		return view;
	}

	/**
	* Get {@link #mazeSearch}
	*/
	@Override
	public MazeSearch getMazeSearch() {
		return mazeSearch;
	}

	/**
	* Get {@link #model}
	*/
	@Override
	public MazeModel getModel() {
		return model;
	}

	public static void main(String[] args){
		new SwingController(Mazes.maze4);
	}
}
