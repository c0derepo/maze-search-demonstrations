package simple_search.swing;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import simple_search.common.CellModel;
import simple_search.common.CellView;
import simple_search.common.MazeModel;
import simple_search.common.View;

/**
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class SwingView extends View{

	private static final Color BORDER_COLOR = Color.DARK_GRAY, WALL = Color.BLACK, PATH = Color.BLUE,
			EMPTY = Color.WHITE, VISITED_EMPTY = Color.LIGHT_GRAY, SOURCE = Color.GREEN, TARGET = Color.RED;
	private static final Color MAZE_COLOR = Color.DARK_GRAY;
	private final JPanel panel, grid;

	private final JButton search;
	private final JTextField info;
	private final JComboBox<String> searchTypes;

	protected SwingView(MazeModel model) {

		super(model);

		grid = new JPanel();
		grid.setLayout(new GridLayout(cells.length, cells[0].length, GAP, GAP));
    	grid.setBorder(new LineBorder(Color.RED, BORDER_WIDTH));
    	grid.setBackground(MAZE_COLOR);

    	makeCellView();

		searchTypes = new JComboBox<>();
		search = new JButton("Start");
		info = new JTextField(20);
		JPanel controlPanel = new JPanel();
		controlPanel.add(searchTypes);
		controlPanel.add(search);
		controlPanel.add(info);

		panel = new JPanel(new BorderLayout(GAP, GAP));
		panel.add(grid, BorderLayout.CENTER);
		panel.add(controlPanel,	BorderLayout.SOUTH);
	}

	protected void makeCellView() {
		SwingCellView[][] cellViews = new SwingCellView[cells.length][cells[0].length];
		for(int row=0; row <cellViews.length; row++) {
			for(int col=0; col<cellViews[row].length; col++) {
				SwingCellView swingCellView = new SwingCellView(cells[row][col]);
				cellViews[row][col] = swingCellView;
				getGridPanel().add(swingCellView);
			}
		}
	}

	public void addActionListener(ActionListener l){
		search.addActionListener(l);
	}

	public void showGui() {

		JFrame f = new JFrame("Maze Search Demo");
		f.setLocationByPlatform(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(panel);
		f.pack();
		f.setVisible(true);
	}

	/**
	* Get value selected in {@link #searchTypes}
	*/
	@Override
	public String getSearchType() {
		return (String) searchTypes.getSelectedItem();
	}

	/**
	* Set values to {@link #searchTypes}
	*/
	@Override
	public void setSearchTypes(String[] values) {
		searchTypes.setModel(new DefaultComboBoxModel<>(values));
		searchTypes.setSelectedIndex(0);
	}

	@Override
	public void info(String text){
		info.setText(text);
	}

	@Override
	public void clearInfo(){
		info.setText("");
	}

	protected JPanel getGridPanel() {
		return grid;
	}

	public class SwingCellView extends JLabel implements CellView{

	    private static final int SPOT_SIZE_FACTOR = 4;
		private final CellModel cellModel;

		protected SwingCellView(CellModel cellModel) {
			this.cellModel = cellModel;
			cellModel.addPropertyChangeListener(e->modelChanged());

			setPreferredSize(new Dimension(SIZE , SIZE));
			setBorder(BorderFactory.createLineBorder(BORDER_COLOR, BORDER_WIDTH));
			setBackground(isWall() ? WALL : EMPTY);
			setOpaque(true);
		}

		@Override
	    public void paintComponent(Graphics g)   {
			g.setColor(bgColorByType());
			super.paintComponent(g);

			if(isWall()) return; //no spot on wall

			int x = getWidth()/SPOT_SIZE_FACTOR;
			int y = getHeight()/SPOT_SIZE_FACTOR;

			int width = getWidth() - 2*x;
			int height = getHeight() - 2*y;

			g.setColor(spotColorByType());
		    g.fillOval(x, y, width , height);
	    }

		private void modelChanged() {
			setBackground(isWall() ? WALL : EMPTY);
			repaint();
		}

		protected Color spotColorByType() {

			if(cellModel.isSource()) return SOURCE;
			if(cellModel.isTarget()) return TARGET;
			if(cellModel.isPath()) return PATH;
			return cellModel.isVisited() ? VISITED_EMPTY : EMPTY;
		}

		private Color bgColorByType() {
			return isWall()  ? WALL : EMPTY;
		}

		@Override
		public String toString() {
			return cellModel.toString() ;
		}

		/* (non-Javadoc)
		 * @see simple_search.common.CellView#getCellModel()
		 */
		@Override
		public CellModel getCellModel() {
			return cellModel;
		}
	}
}



