package simple_search.javafx;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import simple_search.common.CellModel;
import simple_search.common.CellView;
import simple_search.common.MazeModel;
import simple_search.common.View;
/**
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class JavaFxView extends View{

	private static final String MAZE_COLOR = "darkgrey", BORDER_COLOR = "red";
	private final Pane panel;
	private final GridPane grid;
	private final Button search;
	private final TextField info;
	private final ComboBox<String> searchTypes;

	public JavaFxView(MazeModel model) {
		super(model);
		Insets insets = new Insets(2*GAP);

		grid = new GridPane();
		BorderPane.setMargin(grid, insets);
		grid.setHgap(GAP);grid.setVgap(GAP);
		grid.setStyle("-fx-border-color:"+ BORDER_COLOR +"; -fx-border-width:"
				+ BORDER_WIDTH +"; -fx-background-color:"+ MAZE_COLOR +";");
		makeCellView();

		searchTypes = new ComboBox<>();
		search = new Button("Start");
		FlowPane.setMargin(search, insets);
		info = new TextField();
		info.setPrefColumnCount(20);
		FlowPane.setMargin(search, insets);
		Pane controlPanel = new FlowPane(searchTypes,search,info);
		BorderPane.setMargin(grid, insets);

		panel = new BorderPane(grid, null, null, controlPanel, null);
	}

	public void addActionListener(EventHandler<ActionEvent> l){
		search.setOnAction(l);
	}

	protected void makeCellView() {
		JavaFxCellView[][] cellViews = new JavaFxCellView[cells.length][cells[0].length];
		for(int row=0; row <cellViews.length; row++) {
			for(int col=0; col<cellViews[row].length; col++) {
				JavaFxCellView javaFxCellView = new JavaFxCellView(cells[row][col]);
				cellViews[row][col] = javaFxCellView;
				grid.getChildren().add(javaFxCellView);
				GridPane.setConstraints(javaFxCellView, col, row);
			}
		}
	}

	public void showGui(Stage stage) {

		stage.setTitle("Maze Search Demo");
		stage.setScene(new Scene(panel));
		stage.show();
	}

	/**
	 * Get value selected in {@link #searchTypes}
	 */
	@Override
	public String getSearchType() {
		return searchTypes.getSelectionModel().getSelectedItem();
	}

	/**
	 * Set values to {@link #searchTypes}
	 */
	@Override
	public void setSearchTypes(String[] values) {
		searchTypes.setItems(FXCollections.observableArrayList(values));
		searchTypes.getSelectionModel().select(0);
	}

	@Override
	public void info(String text){
		info.setText(text);
	}

	@Override
	public void clearInfo(){
		info.setText("");
	}

	protected Pane getGridPanel() {
		return grid;
	}

	public class JavaFxCellView extends Label implements CellView {

		private static final String BORDER_COLOR = "darkgrey", WALL = "black", EMPTY = "white", PATH = "blue",
				VISITED_EMPTY = "lightgrey", SOURCE = "green", TARGET = "red";
		private final String border = "-fx-border-color:"+ BORDER_COLOR +"; -fx-border-width:"+ BORDER_WIDTH+";";
		private static final int SPOT_SIZE_FACTOR = 5;
		private final CellModel cellModel;
		private final Circle circle;

		protected JavaFxCellView(CellModel cellModel) {
			this.cellModel = cellModel;
			cellModel.addPropertyChangeListener(e->modelChanged());
			setPrefSize(SIZE , SIZE);

			setAlignment(Pos.CENTER);
			circle = new Circle(0, 0, SIZE/SPOT_SIZE_FACTOR);
			circle.radiusProperty().bind(Bindings.min(widthProperty(), heightProperty()).divide(SPOT_SIZE_FACTOR));
			setGraphic(circle);
			setStyle();
		}

		private void setStyle(){
			setStyle(border+ "-fx-background-color:"+ bgColorByType() +";");
			circle.setFill(Color.web(spotColorByType()));
		}

		private void modelChanged() {
			setStyle();
		}

		protected String spotColorByType() {
			if(cellModel.isWall()) return WALL;
			if(cellModel.isSource()) return SOURCE;
			if(cellModel.isTarget()) return TARGET;
			if(cellModel.isPath()) return PATH;
			return cellModel.isVisited() ? VISITED_EMPTY : EMPTY;
		}

		private String bgColorByType() {
			return isWall()  ? WALL : EMPTY;
		}

		@Override
		public String toString() {
			return cellModel.toString() ;
		}

		/* (non-Javadoc)
		 * @see simple_search.common.CellView#getCellModel()
		 */
		@Override
		public CellModel getCellModel() {
			return cellModel;
		}
	}
}