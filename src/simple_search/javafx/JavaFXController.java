package simple_search.javafx;

import common.Mazes;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import simple_search.common.MazeModel;
import simple_search.common.MazeSearch;
import simple_search.common.SearchController;
import simple_search.common.View;

/**
 *
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public class JavaFXController extends Application implements SearchController{

	private JavaFxView view;
	private final MazeModel model;
	private final MazeSearch mazeSearch;

	JavaFXController(int[][] maze)	{
		model = new MazeModel(maze);
		mazeSearch = new MazeSearch(model);
		mazeSearch.setDelay(100);
	}

	@Override
	public void start(Stage stage)	{

		view = new JavaFxView(model);
		view.setSearchTypes(searchTypeNames());
		view.addActionListener(e->search());
		view.showGui(stage);
	}

	@Override
	public void updateInfo(String text){
		Platform.runLater(()-> view.info(text));
	}

	@Override
	public void clearInfo(){
		Platform.runLater(()-> view.clearInfo());
	}

	/**
	* Get {@link #view}
	*/
	@Override
	public View getView() {
		return view;
	}

	/**
	* Get {@link #mazeSearch}
	*/
	@Override
	public MazeSearch getMazeSearch() {
		return mazeSearch;
	}

	/**
	* Get {@link #model}
	*/
	@Override
	public MazeModel getModel() {
		return model;
	}

	public static void main(String[] args){

		JavaFXController controller = new JavaFXController(Mazes.maze4);
		Platform.runLater(() ->controller.start(new Stage()));
	}
}
