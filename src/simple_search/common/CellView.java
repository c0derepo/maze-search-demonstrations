package simple_search.common;

public interface CellView  {

    int SIZE = 30, BORDER_WIDTH = 1;

	default int getRow() {
		return getCellModel().getRow();
	}

	default int getColumn() {
		return getCellModel().getColumn();
	}

	default void setWall() {
		getCellModel().setWall();
	}

	default boolean isWall() {
		return getCellModel().isWall();
	}

	CellModel getCellModel();
}

