package simple_search.common;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MazeSearch implements Runnable{

	public enum SearchType {BFS, DFS, GUI_TEST};
	private SearchType searchType = SearchType.BFS;

	private static final long DEFAULT_DELAY = 1000;
	private long delay = DEFAULT_DELAY;
	private final MazeModel model;
	private List<CellModel> path;
	private volatile boolean isRunning;

	private Thread thread;

	public MazeSearch(MazeModel model) {
		this.model = model;
		isRunning = false;
	}

	@Override
	public void run() {

		isRunning = true;
		if(searchType != SearchType.GUI_TEST) {
			path = solve();
		} else{
			testGui();
		}
		isRunning = false;
	}

	public boolean execute(){
		if(isRunning){
			System.err.println("Search is already running");
			return false;
		}

		//else
		isRunning = true;

		thread = new Thread(this);
		thread.start();
		return true;
	}

	protected List<CellModel> solve() {

		List<CellModel> path = new ArrayList<>();

		CellModel source = getModel().getSource();
		//if cell is wall, or is/was explored or in path
		if(! isToBeExplored(source)) 	return path;

		//stack holds nodes to be explored
		final ArrayDeque<CellModel> stack = new ArrayDeque<>(); //initialize stack
		stack.add(source);
		source.setVisited(true);

		while (! stack.isEmpty()) {

			//pop cell (last element, the tail) from stack
			CellModel cellModel = getSearchType() == SearchType.DFS ? stack.removeLast() : stack.removeFirst();

			if(isSolved(cellModel))	{
				path = getAllParents(cellModel);
				markPath(path);
				return path;
			}
			cellModel.setVisited(true);

			try {
				Thread.sleep(getDelay()); //simulate long process
			} catch (InterruptedException ex) { ex.printStackTrace();}

			final List<CellModel> nb = getModel().getNeighbors(cellModel);
			Collections.shuffle(nb);

			//loop over neighbors
			for(final CellModel nextCellModel : nb){
				if(! isToBeExplored(nextCellModel))  { continue; }
				nextCellModel.setParent(cellModel);
				stack.add(nextCellModel);
			}
		}
		//no solution found
		return path;
	}

	protected List<CellModel> getAllParents(CellModel cellModel) {
		List<CellModel> parents = new ArrayList<>();
		parents.add(cellModel);
		CellModel parent = cellModel.getParent();
		while (parent != null){
			parents.add(parent);
			parent = parent.getParent();
			cellModel.setParent(null);
		}
		Collections.reverse(parents);
		return parents;
	}

	protected void markPath(List<CellModel> path) {
		for(CellModel cellModel : path){
			cellModel.setPath(true);
		}
	}

	protected boolean isSolved(CellModel cellModel){
		return model.getTarget().equals(cellModel);
	}

	/**
	 * Does cell needs to be explored<br/>
	 * (not a wall and not yet explored)
	 */
	protected boolean isToBeExplored(final CellModel cellModel) {
		//if cell is wall, or is/was explored or in path
		return cellModel != null && ! cellModel.isWall() && ! cellModel.isVisited()  ;
	}

	void testGui() {

		final Random rand = new Random();
		long counter = 0, limit = 20*1000/Math.max(50, delay); //limit loop to 20 seconds
		while (counter++ < limit){ // loop for demonstration

			int row = rand.nextInt(model.getRows());
			int col = rand.nextInt(model.getColumns());
			CellModel cellModel = model.cellModel(row, col);
			configure(cellModel, rand);

			try {
				Thread.sleep(Math.max(50, delay)); //simulate long process
			} catch (InterruptedException ex) { ex.printStackTrace();}
		}
	}

	protected void configure(CellModel cellModel, Random rand){

		int option = rand.nextInt(6);
		switch (option){
		default: case 0:
			cellModel.setEmpty();
			break;
		case 1:
			cellModel.setWall();
			break;
		case 2:
			cellModel.setSource();
			break;
		case 3:
			cellModel.setTarget();
			break;
		case 4:
			cellModel.setPath(rand.nextBoolean());
			break;
		case 5:
			cellModel.setVisited(rand.nextBoolean());
			break;
		}
	}

	public SearchType getSearchType() {
		return searchType;
	}

	public MazeSearch setSearchType(SearchType searchType) {
		this.searchType = searchType;
		return this;
	}

	protected long getDelay() {
		return delay;
	}

	public MazeSearch setDelay(long delay) {
		this.delay = delay;
		return this;
	}

	public MazeModel getModel() {
		return model;
	}

	public List<CellModel> getPath() throws InterruptedException  {

		while (isRunning()){
			//wait for the search to end
		}
		return path;
	}

	public synchronized boolean isRunning() {
		return isRunning;
	}
}