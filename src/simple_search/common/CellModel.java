package simple_search.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class CellModel{

	public enum CellType {EMPTY,WALL,SOURCE,TARGET, STATION}

    private final int row, column;
    private boolean isVisited, isPath;
    private CellModel parent;
    private CellType type;

    private final PropertyChangeSupport pcs;

    public CellModel(int row, int column)  {
       this(row, column, false);
    }

	public CellModel(int row, int column, boolean isWall) {
		this.row = row;
		this.column = column;
		type = isWall ? CellType.WALL : CellType.EMPTY;
		isVisited = false;
		isPath = false;
		parent = null;
		pcs = new PropertyChangeSupport(this);
	}

	protected void init(){
		setVisited(false);
		setPath(false);
		parent = null;
	}

	protected void changeType(CellType type){
		Object old = this.type;
    	this.type = type;
		pcs.firePropertyChange("Type", old, this.type);
	}

	public void addPropertyChangeListener(PropertyChangeListener l) {
		pcs.addPropertyChangeListener(l);
	}

	public void removePropertyChangeListener(PropertyChangeListener l) {
		pcs.removePropertyChangeListener(l);
	}

	public int getRow() {  return row; }
	public int getColumn() {return column; }

	public boolean isEmpty() { return type == CellType.EMPTY; }
    public void setEmpty() { changeType(CellType.EMPTY);}

    public boolean isWall() { return type == CellType.WALL; }
    public void setWall() { changeType(CellType.WALL);}

    public boolean isSource() { return type == CellType.SOURCE; }
    public void setSource() { changeType(CellType.SOURCE);}

    public boolean isTarget() { return type == CellType.TARGET; }
    public void setTarget(){ changeType(CellType.TARGET);}

    public boolean isPath() { return isPath; }
    public void setPath(boolean isPath) {
    	Object old = this.isPath;
    	this.isPath = isPath;
		pcs.firePropertyChange("Path", old, isPath);
    }

    public boolean isVisited() { return isVisited; }
    public void setVisited(boolean isVisited) {
		Object old = this.isVisited;
		this.isVisited = isVisited;
		pcs.firePropertyChange("Visited", old, isVisited);
	}

    public CellModel getParent() {
		return parent;
	}
	public void setParent(CellModel parent) {
		this.parent = parent;
	}

	protected  CellType getType(){
		return type;
	}

	@Override
	public String toString() {
		return  type + " ["+ row + "-" + column +"]";
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof CellModel)) return false;
		CellModel other = (CellModel)obj;
		return row == other.getRow() && column == other.getColumn() && type == other.type;
	}
}