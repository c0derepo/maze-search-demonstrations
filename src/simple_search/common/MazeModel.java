package simple_search.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MazeModel{

	/**
	 *The underlying array of the model.
	 *It not exposed so it can not be changed by clients.
	 */
	private final CellModel[][] maze;
	private final int rows, columns;
	private CellModel target, source;

	private static final int[][] DIRECTIONS = {
			{ 0 ,-1}, // north
			{ 0 , 1}, // south
			{ 1 , 0}, // east
			{-1 , 0}, // west
	};

	protected final int[][] mazeRepresentation;

	public MazeModel(int[][] mazeRepresentation) {

		this.mazeRepresentation = mazeRepresentation;
		rows = mazeRepresentation.length;
		columns = mazeRepresentation[0].length;
		maze = new CellModel[rows][columns];

		constructCells();
	}

	protected void constructCells() {
		for(int row = 0; row < rows; row++ ){
			for(int col = 0; col < columns; col++){
				maze[row][col] = configureCellModel(new CellModel(row, col), mazeRepresentation[row][col]);
			}
		}
	}

	public void resetModel() {
		for(int row = 0; row < rows; row++ ){
			for(int col = 0; col < columns; col++){
				if(maze[row][col] != null) {
					configureCellModel(maze[row][col], mazeRepresentation[row][col]);
					maze[row][col].init();
				}
			}
		}
	}

	public List<CellModel> getNeighbors(CellModel cellModel) {

		List<CellModel> neigbours = new ArrayList<>();
		for(int[] direction : DIRECTIONS){
			int newRow = cellModel.getRow() + direction[0];
			int newCol = cellModel.getColumn() + direction[1];
			if(isValid(newRow, newCol)){
				neigbours.add(maze[newRow][newCol]);
			}
		}
		return neigbours;
	}

	protected <T extends CellModel> CellModel configureCellModel( T cell, int type) {

		switch (type){
			default: case 0:
				cell.setEmpty();
				break;
			case 1:
				cell.setWall();
				break;
			case 2:
				cell.setSource();
				source = cell;
				break;
			case 3:
				cell.setTarget();
				target = cell;
				break;
		}
		return cell;
	}

	public Iterator<CellModel> getIterator(){
		return  new CellIterator(rows, columns);
	}

	public CellModel cellModel(int row, int column) {

		return maze[row][column];
	}

	public CellModel getTarget() {
		return target;
	}
	public void setTarget(CellModel target) {
		if(this.target != null){
			this.target.setEmpty();
		}
		this.target = target;
	}

	public CellModel getSource() {
		return source;
	}
	public void setSource(CellModel source) {
		if(this.source != null){
			this.source.setEmpty();
		}
		source.setSource();
		this.source = source;
	}

	public int getRows(){
		return rows;
	}

	public int getColumns(){
		return columns;
	}

	protected CellModel[][] getMaze(){
		return maze;
	}

	boolean isValid(int row, int column){
		return row>=0 && column>=0 	&& row < rows && column < columns;
	}

	class CellIterator implements Iterator<CellModel>{

		private int rowPointer, columnPointer;
		private final int rows, columns;

		CellIterator(int rows, int columns) {
			this.rows = rows;
			this.columns = columns;
			rowPointer = 0; columnPointer = 0;
		}

		@Override
		public boolean hasNext() {
			return rowPointer < rows && columnPointer < columns;
		}

		/* (non-Javadoc)
		 * @see java.util.Iterator#next()
		 */
		@Override
		public CellModel next() {

			CellModel cellModel = null;
			if(hasNext()){
				cellModel = maze[rowPointer][columnPointer];
				step();
			}
			return cellModel;
		}

		private void step() {
			if(columnPointer < columns - 1) {
				columnPointer++;
			} else if(rowPointer < rows - 1) {
				columnPointer=0;
				rowPointer++;
			}else{
				rowPointer = rows; columnPointer = columns; //riched end
			}
		}
	}
}