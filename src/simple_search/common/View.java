package simple_search.common;

import java.util.Iterator;


/**
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public abstract class View {

	protected static final int BORDER_WIDTH = 2, GAP = 2;
	protected CellModel[][] cells;

	protected View(MazeModel model) {

		cells = new CellModel[model.getRows()][model.getColumns()];

    	Iterator<CellModel> iterator = model.getIterator();
    	while(iterator.hasNext()){
    		CellModel cellModel = iterator.next();
    		cells[cellModel.getRow()][cellModel.getColumn()] = cellModel;
    	}
	}

	/**
	* Get value selected in {@link #searchTypes}
	*/
	public abstract String getSearchType();

	/**
	* Set values to {@link #searchTypes}
	*/
	public abstract void setSearchTypes(String[] values);

	public abstract void info(String text);

	public abstract void clearInfo();
}
