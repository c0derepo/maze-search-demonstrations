package simple_search.common;

import java.util.List;
import java.util.stream.Stream;

import simple_search.common.MazeSearch.SearchType;

/**
 *
 *
 * @author c0der
 * 25 Jun 2020
 *
 */
public interface SearchController{

	void updateInfo(String text);
	void clearInfo();

	View getView();
	MazeSearch getMazeSearch();
	MazeModel getModel();

	default void search() {

		if( getMazeSearch().isRunning()) {
			getView().info("Search is already running");
			return;
		}
		getModel().resetModel();
		clearInfo();
		getMazeSearch().setSearchType(SearchType.valueOf(getView().getSearchType()));
		updateInfo("Running "+ getMazeSearch().getSearchType());
		getMazeSearch().execute();
		processResult();
	}

	default void processResult() {

		new Thread(()->{
			List<CellModel> path = null;

			try {
				path = getMazeSearch().getPath();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			if(getMazeSearch().getSearchType() == SearchType.GUI_TEST){
				updateInfo("Test completed");
			}else if(path == null || path.isEmpty()){
				updateInfo("No path found");
			}else{
				updateInfo("Path length "+ path.size());
			}
		}).start();
	}

	default String[] searchTypeNames(){
		return Stream.of(SearchType.values()).map(v->v.name()).toArray(String[]::new);
	}
}
