/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_with_stations.swing;

import java.util.List;

import javax.swing.SwingUtilities;

import common.Mazes;
import search_with_stations.commom.CellSupportsStations;
import search_with_stations.commom.MazeModelSupportStationStation;
import search_with_stations.commom.MazeSearchSupportStations;
import search_with_stations.commom.SearchControlSupportStations;
import simple_search.common.CellModel;
import simple_search.common.View;

public class SwingControllerSupportStations implements SearchControlSupportStations{

	private final SwingViewSupportStations view;
	private final MazeModelSupportStationStation model;
	private MazeSearchSupportStations mazeSearch;

	private List<CellSupportsStations> visitedStations;
	private List<CellModel> accumulatedPath;
	private int numberOfStations;

	SwingControllerSupportStations(int[][] maze)	{
		model = new MazeModelSupportStationStation(maze);
		view = new SwingViewSupportStations(model);
		view.setSearchTypes(searchTypeNames());
		view.addActionListener(e->search());
		view.showGui();
	}

	@Override
	public void updateInfo(String text){
		SwingUtilities.invokeLater(()-> getView().info(text));
	}

	@Override
	public void clearInfo(){
		SwingUtilities.invokeLater(()-> getView().clearInfo());
	}

	/**
	 * Get {@link #view}
	 */
	@Override
	public View getView() {
		return view;
	}

	/**
	 * Get {@link #mazeSearch}
	 */
	@Override
	public MazeSearchSupportStations getMazeSearch() {
		return mazeSearch;
	}

	/**
	 * Set {@link #mazeSearch}
	 */
	@Override
	public void setMazeSearch(MazeSearchSupportStations mazeSearch) {
		 this.mazeSearch =  mazeSearch;
	}

	/**
	 * Get {@link #model}
	 */
	@Override
	public MazeModelSupportStationStation getModel() {
		return model;
	}

	/**
	* Get {@link #visitedStations}
	*/
	@Override
	public List<CellSupportsStations> getVisitedStations() {
		return visitedStations;
	}

	/**
	* Set {@link #visitedStations}
	*/
	@Override
	public void setVisitedStations(List<CellSupportsStations> visitedStations) {
		this.visitedStations = visitedStations;
	}

	/**
	* Get {@link #numberOfStations}
	*/
	@Override
	public int getNumberOfStations() {
		return numberOfStations;
	}

	/**
	* Set {@link #numberOfStations}
	*/
	@Override
	public void setNumberOfStations(int numberOfStations) {
		this.numberOfStations = numberOfStations;
	}

	/**
	* Get {@link #accumulatedPath}
	*/
	@Override
	public List<CellModel> getAccumulatedPath() {
		return accumulatedPath;
	}

	/**
	* Set {@link #accumulatedPath}
	*/
	@Override
	public void setAccumulatedPath(List<CellModel> accumulatedPath) {
		this.accumulatedPath = accumulatedPath;
	}

	public static void main(String[] args){
		new SwingControllerSupportStations(Mazes.maze4);
	}
}
