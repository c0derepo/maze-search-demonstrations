package search_with_stations.swing;

import java.awt.Color;

import search_with_stations.commom.CellSupportsStations;
import search_with_stations.commom.MazeModelSupportStationStation;
import simple_search.common.CellModel;
import simple_search.swing.SwingView;


public class SwingViewSupportStations extends SwingView {

	protected static final Color STATION = Color.YELLOW.brighter(), VISITED_STATION = Color.YELLOW.darker();

    public SwingViewSupportStations(MazeModelSupportStationStation model) {
    	super(model);
    }

	@Override
	protected void makeCellView() {
		SwingCellView[][] cellViews = new SwingCellView[cells.length][cells[0].length];
		for(int row=0; row <cellViews.length; row++) {
			for(int col=0; col<cellViews[row].length; col++) {
				SwingCellView swingCellView = new SwingCellViewSupportStations(cells[row][col]);
				cellViews[row][col] = swingCellView;
				getGridPanel().add(swingCellView);
			}
		}
	}

	class SwingCellViewSupportStations  extends SwingCellView{

		SwingCellViewSupportStations(CellModel cellModel) {
			super(cellModel);
		}

		@Override
		protected Color spotColorByType(){

			if(getCellModel() instanceof CellSupportsStations &&
					((CellSupportsStations)getCellModel()).isStation())
									return getCellModel().isVisited() ? VISITED_STATION : STATION;
			return super.spotColorByType();
		}
	}
}
