package search_with_stations.javafx;

import javafx.scene.layout.GridPane;
import search_with_stations.commom.CellSupportsStations;
import search_with_stations.commom.MazeModelSupportStationStation;
import simple_search.common.CellModel;
import simple_search.javafx.JavaFxView;

public class JavaFxViewSupportStations extends JavaFxView {

	protected static final String STATION = "yellow", VISITED_STATION = "gold";

    public JavaFxViewSupportStations(MazeModelSupportStationStation model) {
    	super(model);
    }

	@Override
	protected void makeCellView() {
		JavaFxCellView[][] cellViews = new JavaFxCellView[cells.length][cells[0].length];
		for(int row=0; row <cellViews.length; row++) {
			for(int col=0; col<cellViews[row].length; col++) {
				JavaFxCellView javaFxCellView = new JavaFxCellViewSupportStations(cells[row][col]);
				cellViews[row][col] = javaFxCellView;
				getGridPanel().getChildren().add(javaFxCellView);
				GridPane.setConstraints(javaFxCellView, col, row);
			}
		}
	}

	class JavaFxCellViewSupportStations  extends JavaFxCellView{

		JavaFxCellViewSupportStations(CellModel cellModel) {
			super(cellModel);
		}

		@Override
		protected String spotColorByType(){

			if(getCellModel() instanceof CellSupportsStations &&
					((CellSupportsStations)getCellModel()).isStation())
									return getCellModel().isVisited() ? VISITED_STATION : STATION;
			return super.spotColorByType();
		}
	}
}
