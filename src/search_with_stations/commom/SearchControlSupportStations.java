/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_with_stations.commom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import simple_search.common.CellModel;
import simple_search.common.MazeSearch.SearchType;
import simple_search.common.SearchController;

/**
 *
 * @author c0der
 * 5 Jul 2020
 *
 */
public interface SearchControlSupportStations extends SearchController {

	long STEP_DELAY = 100, SEGMENT_DELAY = 2000;

	@Override
	MazeSearchSupportStations getMazeSearch();
	void setMazeSearch(MazeSearchSupportStations mazeSearch );

	@Override
	MazeModelSupportStationStation getModel();

	List<CellSupportsStations> getVisitedStations();
	void setVisitedStations(List<CellSupportsStations> visitedStations);

	int getNumberOfStations();
	void setNumberOfStations(int numberOfVisitedStations);

	List<CellModel> getAccumulatedPath();
	void setAccumulatedPath(List<CellModel> accumulatedPath);

	@Override
	default void search() {

		if( getMazeSearch() != null && getMazeSearch().isRunning()) {
			getView().info("Search is already running");
			return;
		}
		setNumberOfStations(getModel().getStations().size());
		getModel().resetModel();
		clearInfo();

		setVisitedStations(new ArrayList<>());
		setAccumulatedPath(new ArrayList<>());
		searchSegment();
	}

	default void searchSegment(){

		setMazeSearch(new MazeSearchSupportStations(getModel()));

		getMazeSearch().setIsSolevedTest(
				c->{
					return   isAllStationsFound() ?
							 c.isTarget() : isUnVisitedStation((CellSupportsStations) c, getVisitedStations()) ;
				}
		).setDelay(STEP_DELAY)
		.setSearchType(SearchType.valueOf(getView().getSearchType()));
		updateInfo("Running "+ getMazeSearch().getSearchType());
		getMazeSearch().execute();
		processResult();
	}

	@Override
	default void processResult() {

		new Thread(()->{
			List<CellModel> path = null;

			try {
				path = getMazeSearch().getPath();
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
			if(getMazeSearch().getSearchType() == SearchType.GUI_TEST){
				updateInfo("Test completed");
			}else if(path == null || path.isEmpty()){
				updateInfo("No path found");
			}else{
				getAccumulatedPath().addAll(new ArrayList<>(path));

				if(isAllStationsFound() && getMazeSearch().isSoleved(getAccumulatedPath().get(getAccumulatedPath().size() -1 ))) {
					updateModel();
					updateInfo("Path length "+ getAccumulatedPath().size());
				}else{
					updateInfo("Segment path length "+ path.size());
					path.forEach(c->c.setPath(true));
					try {
						Thread.sleep(SEGMENT_DELAY);
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					}
					updateModel(path.get(path.size()-1));
					searchSegment();
				}
			}
		}).start();
	}

	private boolean isUnVisitedStation(CellSupportsStations cellModel, Collection<CellSupportsStations> visitedStations) {

		if( cellModel.isStation() &&  ! visitedStations.contains(cellModel)){
			visitedStations.add(cellModel);
			return true;
		}
		return false;
	}

	private boolean isAllStationsFound() {
		return getVisitedStations().size() >= getNumberOfStations();
	}

	private void updateModel() {

		getModel().resetModel();

		for(CellModel cellModel : getAccumulatedPath()){
			cellModel.setPath(true);
		}

		for( CellSupportsStations cell : getVisitedStations()){
			cell.setVisited(true);
		}
	}

	private void updateModel(CellModel newSource) {

		getModel().resetModel();
		getModel().setSource(newSource);
	}
}
