package search_with_stations.commom;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

import simple_search.common.CellModel;
import simple_search.common.MazeSearch;

public class MazeSearchSupportStations extends MazeSearch{

	private Predicate<CellModel> isSolevedTest;

	public MazeSearchSupportStations(MazeModelSupportStationStation model) {
		super(model);
		isSolevedTest = cell->cell.equals(model.getTarget());
	}

	@Override
	protected List<CellModel> solve() {

		List<CellModel> path = new ArrayList<>();

		CellModel source = getModel().getSource();
		//if cell is wall, or is/was explored or in path
		if(! isToBeExplored(source)) 	return path;

		//stack holds nodes to be explored
		final ArrayDeque<CellModel> stack = new ArrayDeque<>(); //initialize stack
		stack.add(source);
		source.setVisited(true);

		while (! stack.isEmpty()) {

			//pop cell (last element, the tail) from stack
			CellModel cellModel = getSearchType() == SearchType.DFS ? stack.removeLast() : stack.removeFirst();

			if(isSolved(cellModel))	{
				path = getAllParents(cellModel);
				markPath(path);
				return path;
			}
			cellModel.setVisited(true);

			try {
				Thread.sleep(getDelay());
			} catch (InterruptedException ex) { ex.printStackTrace();}

			final List<CellModel> nb = getModel().getNeighbors(cellModel);
			Collections.shuffle(nb);

			//loop over neighbors
			for(final CellModel nextCellModel : nb){
				if(! isToBeExplored(nextCellModel))  { continue; }
				nextCellModel.setParent(cellModel);
				stack.add(nextCellModel);
			}
		}
		//no solution found
		return path;
	}

	@Override
	protected void configure(CellModel cellModel, Random rand){
		int option = rand.nextInt(7);
		if(option == 6 && cellModel instanceof CellSupportsStations) {
			((CellSupportsStations) cellModel).setStation();
		} else{
			super.configure(cellModel, rand);
		}
	}

	@Override
	protected boolean isSolved(CellModel cellModel){
		return isSolevedTest.test(cellModel);
	}

	public Predicate<CellModel> getIsSolevedTest() {
		return isSolevedTest;
	}

	public boolean isSoleved(CellModel c) {
		return isSolevedTest.test(c);
	}

	public MazeSearchSupportStations setIsSolevedTest(Predicate<CellModel> isSolevedTest) {
		this.isSolevedTest = isSolevedTest;
		return this;
	}
}