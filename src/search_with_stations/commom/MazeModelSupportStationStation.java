package search_with_stations.commom;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import simple_search.common.CellModel;
import simple_search.common.MazeModel;

public class MazeModelSupportStationStation extends MazeModel{

	public MazeModelSupportStationStation(int[][] mazeRepresentation) {
		super(mazeRepresentation);
    }

	@Override
	protected void constructCells() {
		for(int row = 0; row < getRows(); row++ ){
			for(int col = 0; col < getColumns(); col++){
				getMaze()[row][col] = configureCellModel(new CellSupportsStations(row, col),
																	mazeRepresentation[row][col]);
			}
		}
	}

	@Override
	protected <T extends CellModel> CellModel configureCellModel(T cell, int type) {

		super.configureCellModel(cell, type);

		if(cell instanceof CellSupportsStations && type == 4){
			((CellSupportsStations) cell).setStation();
		}
		return cell;
	}

	public List<CellSupportsStations> getStations(){
		return Stream.of(getMaze()).
				flatMap(c -> Stream.<CellModel>of(c)).
				map(c->(CellSupportsStations)c).
				filter(c-> c.isStation()).
				collect(Collectors.toList());
	}
}
