/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package search_with_stations.commom;

import simple_search.common.CellModel;

public class CellSupportsStations extends CellModel{

	public CellSupportsStations(int row, int column)  {
		this(row, column, false);
	}

	public CellSupportsStations(int row, int column, boolean isWall) {
		super(row, column, isWall);
	}

    public boolean isStation() { return getType() == CellType.STATION; }
    public void setStation(){ changeType(CellType.STATION);}
}
